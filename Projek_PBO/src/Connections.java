
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

// bisa juga diimpor sekaligus seperti ini:
// import java.sql.*

public class Connections {

    // Menyiapkan paramter JDBC untuk koneksi ke datbase
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/brickbreaker";
    static final String USER = "root";
    static final String PASS = "";

    // Menyiapkan objek yang diperlukan untuk mengelola database
    private Connection connect;
    static Statement stmt;
    static ResultSet rs;

    public Connection getConnections() {
        if (connect == null) {
            try {
                Class.forName(JDBC_DRIVER);
                try {
                    connect = DriverManager.getConnection(DB_URL, USER, PASS);
                } catch (SQLException ex) {
                    Logger.getLogger(Connections.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Connections.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return connect;
    }

}