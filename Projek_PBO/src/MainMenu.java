import java.awt.*;
import java.awt.event.*;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;

import java.util.Scanner;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class MainMenu extends JFrame implements ActionListener {

	// MainMenu
	// Allows for user to play, ask for help, or exit the game
	MyButton start, score, help, exit; // Buttons for user interaction
	MyButton[] buttons; // Arroy of all buttons
//	Sound mainSound;

	JLabel bg; // Background image

	public MainMenu() {

		// Basic setup of screen
		// Positions and sizes are set for all buttons and images

		super("Brick Breaker");
//		try {
//
//			mainSound = new Sound("src\\Resource\\Sounds\\MainSound.wav");
//		} catch (UnsupportedAudioFileException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (LineUnavailableException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (mainSound.status.equals("paused")) {
//			mainSound.play();
//		}
		setLayout(null);
		setSize(800, 600);

		start = new MyButton("start");
		score = new MyButton("score");
		help = new MyButton("help");
		exit = new MyButton("exit");

		start.setSize(170, 65);
		score.setSize(170, 65);
		help.setSize(170, 65);
		exit.setSize(170, 65);

		buttons = new MyButton[] { start, score, help, exit }; // Buttons are put in an Array for easy access and
																// manipulation

		for (int i = 0; i < buttons.length; i++) {
			MyButton b = buttons[i];
			b.setLocation(400 - b.getWidth() / 2, 300 + i * 40);
			b.addActionListener(this);
			add(b);

		}

		bg = new JLabel(new ImageIcon("src\\Resource\\Image\\mainBG.png"));
		bg.setSize(800, 600);
		add(bg);
		dispose();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Program will exit on close
		setVisible(true); // Makes it visible

	}

	public void actionPerformed(ActionEvent e) {

		// Checks for any actions

		Object src = e.getSource(); // Gets source of ActionEvent

		if (src == start) { // Starts game
			dispose();
			new BB();
			setVisible(false); // Stops menu

		} else if (src == score) { // Loads score frame
			dispose();
			new ScoreFrame();
			setVisible(false); // stops menu

		} else if (src == help) { // Loads help frame
			dispose();
			new HelpFrame();
			setVisible(false); // stops menu

		} else if (src == exit) { // Exits program
			System.exit(0);
		}

	}
}

class MyButton extends JButton {

	// Custom JButton class
	// Gets rid of border and fill
	// Puts in custom icon

	public MyButton(String f) {

		ImageIcon p = new ImageIcon("src\\Resource\\Image\\" + f + "P.png"); // Custom icon
		ImageIcon d = new ImageIcon("src\\Resource\\Image\\" + f + "D.png"); // Cutsom icon

		this.setIcon(d); // Sets new icon
		this.setRolloverIcon(p);
		this.setPressedIcon(d);
		this.setContentAreaFilled(false); // Gets rid of default settings
		this.setBorderPainted(false);

	}
}

class HelpFrame extends JFrame implements ActionListener {

	// Displays controls for the game

	JLabel info; // An image that has the controls
	MyButton exit; // Exit button

	public HelpFrame() {

		// Setsup basic position and sizes of all components in the frame

		super();

		setLayout(null);
		setSize(800, 600);

		exit = new MyButton("exit");
		exit.setSize(120, 65);
		exit.setLocation(325, 400);
		exit.addActionListener(this); // allows for be clicked and respond
		add(exit);

		info = new JLabel(new ImageIcon("src\\Resource\\Image\\InstructionBG.png"));
		info.setSize(800, 600);
		add(info);
		dispose();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Progran exits on close
		setVisible(true); // Makes visible

	}

	public void actionPerformed(ActionEvent e) {

		// Displays screen until exited

		Object src = e.getSource(); // gets source of action

		if (src == exit) { // Returns to Main Menu
			dispose();
			new MainMenu();
			setVisible(false);

		}
	}

}

class ScoreFrame extends JFrame implements ActionListener {

	// Displays controls for the game

	JLabel info, no, title, score1, score2, score3, score4, score5; // An image that has the controls
	MyButton exit; // Exit button
	private Connections connect = new Connections();

	public ScoreFrame() {

		// Setsup basic position and sizes of all components in the frame

		super();
		setLayout(null);
		setSize(800, 600);

		exit = new MyButton("exit");
		exit.setSize(120, 65);
		exit.setLocation(325, 500);
		exit.addActionListener(this); // allows for be clicked and respond
		add(exit);
		Font f = new Font("Bebas Neue", Font.PLAIN, 60);
		String[] s = { "", "", "", "", "" };
		try {

			java.sql.Connection conn = connect.getConnections();
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("SELECT * FROM score_board ORDER BY score DESC LIMIT 5");
			int i = 0;
			if (result.next()) {
				s[i] = Integer.toString(result.getInt("score"));
				System.out.println(s[i]);
				i++;
			}

		} catch (SQLException ex) {
			Logger.getLogger(BB.class.getName()).log(Level.SEVERE, null, ex);
		}
		for (String i : s) {
			System.out.println(i);
		}
		score1 = new JLabel(s[0]); // An image that will be displayed
		score1.setFont(f); // Set up
		score1.setForeground(Color.WHITE);
		score1.setSize(250, 80);
		score1.setLocation(350, 250);
		add(score1);
		title = new JLabel("High Score"); // An image that will be displayed
		title.setFont(f); // Set up
		title.setForeground(Color.WHITE);
		title.setSize(300, 80);
		title.setLocation(250, 50);
		add(title);

		info = new JLabel(new ImageIcon("src\\Resource\\Image\\allBG.png"));
		info.setSize(800, 600);

		add(info);
		dispose();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Progran exits on close
		setVisible(true); // Makes visible

	}

	public void actionPerformed(ActionEvent e) {

		// Displays screen until exited

		Object src = e.getSource(); // gets source of action

		if (src == exit) { // Returns to Main Menu

			new MainMenu();
			setVisible(false);

		}
	}

}

class LoseFrame extends JFrame implements ActionListener {

	// Frame displayed when User loses all lives

	JLabel msg, bg; // Two images
	MyButton exit; // Exit button

	public LoseFrame() {

		// Sets up basic position and sizes of components in the frame

		super();

		setLayout(null);
		setSize(800, 600);

		Font f = new Font("Bebas Neue", Font.PLAIN, 60); // Selects font for use

		msg = new JLabel("You lose"); // An image that will be displayed
		msg.setFont(f); // Set up
		msg.setForeground(Color.WHITE);
		msg.setSize(600, 100);
		msg.setLocation(250, 250);
		add(msg);

		exit = new MyButton("exit"); // Exit button set up
		exit.setSize(120, 65);
		exit.setLocation(300, 450);
		exit.addActionListener(this); // Allows button to be clicked and respond
		add(exit);

		bg = new JLabel(new ImageIcon("src\\Resource\\Image\\allBG.png")); // Background image
		bg.setSize(800, 600);
		add(bg);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Program exits on close
		setVisible(true);

	}

	public void actionPerformed(ActionEvent e) {

		// Displays screen until exited

		Object src = e.getSource(); // Finds source of action

		if (src == exit) { // Returns to main menu
			new MainMenu();
			setVisible(false);
		}
	}

}
